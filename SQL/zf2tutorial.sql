/*

Source Server         : localhost_3306
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : zf2tutorial

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2013-07-26 11:31:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `album`
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES ('1', 'StateGrace', '中文测试');
INSERT INTO `album` VALUES ('2', 'StateGrace', '21');
INSERT INTO `album` VALUES ('3', 'StateGrace', 'Wrecking Ball (Deluxe)');
INSERT INTO `album` VALUES ('4', 'StateGrace', 'Born  To  Die');
INSERT INTO `album` VALUES ('5', 'StateGrace', 'Making  Mirrors');
INSERT INTO `album` VALUES ('6', 'StateGrace', '12321');
