<?php
// module/Album/config/module.config.php:
return array (
		// 控制器映射
		'controllers' => array (
				'invokables' => array (
						'Album\Controller\Album' => 'Album\Controller\AlbumController' 
				) 
		),
		
		// 路由器分发(方法)
		'router' => array (
				'routes' => array (
						'album' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/album[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id' => '[0-9]+' 
										),
										'defaults' => array (
												'controller' => 'Album\Controller\Album',
												'action' => 'index' 
										) 
								) 
						) 
				) 
		),
		// 视图管理
		'view_manager' => array (
				'template_path_stack' => array (
						 'album'=> __DIR__ . '/../view',
				) 
		) 
);