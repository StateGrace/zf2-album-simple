<?php
// module/Album/src/Album/Form/AlbumForm.php:
namespace Album\Form;

use Zend\Form\Form;
//构建表单对象
class AlbumForm extends Form
{
	public function __construct($name = null)
	{
		// 我们可以忽略名称传递
		parent::__construct('album');
		$this->setAttribute('method', 'post');
		$this->add(array(
				'name' => 'id',
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'artist',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => '艺术家',
				),
		));
		$this->add(array(
				'name' => 'title',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => '标题',
				),
		));
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
	}
}